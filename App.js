import React, {useState} from 'react';
import { StyleSheet, View, FlatList, Alert } from 'react-native';
import Header from './src/components/header';
import Item from './src/components/items';
import AddItem from'./src/components/addItems';
// --------------------------------------------
import Data from './src/data/sample.json';

export default function App() {

  // array
  const[ todos, setTodos ] = useState(Data)

  //edit item in array
  const editItem = (newText, id) => {
    const editTaskList = todos.map(task => {
      if(task.id === id){
        return { ...task, title: newText }
      }
      return task;
    });
    setTodos(editTaskList);
  }

  // delete item in array
  const deleteItem = (id) =>{
    setTodos((prevTodos) => {
      return prevTodos.filter(todo => todo.key != id)
    })
  }

  // add item in array
  const addItem = (text) =>{
    if(text.length > 0){
      setTodos((prevTodos) =>{
        return[
          { title: text, key: Math.random().toString() },
          ...prevTodos,
        ];
      });
      console.log(Data)
    }else{
      Alert.alert('OOPS!','Must be at least 1 character',[
        {
          text: 'Understood', onPress: () => console.log('alert closed')
        }
      ]);
    }
  } 

  return (
    <View style={styles.container}>
        <Header />
        <View style={styles.content}>
          <AddItem addItem={addItem} />
          <View style={styles.list}>
            <FlatList
              data={todos}
              renderItem={({item})  => (
                <Item key={item.id} item={item} deleteItem={deleteItem} editItem={editItem}/>
              )}
            />
          </View>  
        </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  content: {
    flex: 1,
    padding: 40
  },

  list: {
    flex: 1,
    marginTop: 20
  }
});

import React, { useState } from 'react';
import { StyleSheet, Text, TextInput, View, Button } from 'react-native';

export default function AddTodo({ addItem }) {

	// create array
    const [text, setText] = useState('');

    // create object 
    const inputItem = (val) => {
        setText(val);
        console.log(val);
    }

    return ( 
        <View>
            <TextInput style = { styles.input }
            placeholder = 'new task title...'
            onChangeText = { (val) => inputItem(val) }
            />

            <Button onPress = { () => addItem(text) }
            title = 'Add Task'
            color = 'royalblue'
            /> 
        </View>
    );
}

const styles = StyleSheet.create({
    input: {
        marginBottom: 10,
        paddingHorizontal: 8,
        paddingVertical: 6,
        borderBottomWidth: 1,
        borderBottomColor: '#ddd'
    },
})
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default function Header(){
	return(
		<View style={styles.title}>
			<Text style={styles.titleText}> Todo List </Text>
		</View>
	);
}

const styles = StyleSheet.create({
  title: {
  	backgroundColor: 'royalblue'
  },

  titleText: {
   	textAlign: 'center',
   	fontSize: 20,
   	padding: 16,
   	color: '#fff',
   	fontWeight: 'bold'
  }
});
import React, { useState } from 'react';
import { StyleSheet, TouchableOpacity , Text, View, Button, Alert, Modal, TextInput } from 'react-native';

export default function Items({ item, deleteItem, editItem }){

	const showTaskInfo = () => {
		Alert.alert("Task info",item.title,[
			{
			  text: 'Understood', onPress: () => console.log('alert closed')
			}
		]);
	}

	// create array
    const [text, setText] = useState('');

    // create object 
    const inputItem = (val) => {
        setText(val);
        // console.log(val);
    }

	const [modalOpen, setModalOpen] = useState(false);

	return(
		<>
			<TouchableOpacity onPress={() => showTaskInfo()} >
				<View style={styles.item}>
					<Text style={styles.itemText}> {item.title} </Text>
				</View>
			</TouchableOpacity>
			<Button title="Remove" color="red" onPress={() => deleteItem(item.key)}/>
			
			<Modal visible={modalOpen}>
				<View style={styles.modal}>
					<TextInput style = { styles.input }
						placeholder = {item.title}
						onChangeText = { (val) => inputItem(val) }
					/>
					<Button title="Save" color="green"  onPress={() => editItem(text) }/>
					<Button title="Cancel" color="gray" onPress={() => setModalOpen(false)} />
				</View>
			</Modal>
			
			<Button title="Edit" onPress={() => setModalOpen(true)}/>

			{/* <Button title="Edit" onPress={() => editItem(item.key)}/> */}
		</>

	);
}

const styles = StyleSheet.create({
	item: {
		flexDirection: 'column',
		padding: 16,
		marginTop: 16,
		borderColor:'#bbb',
		borderWidth: 1,
		borderStyle: 'solid',
	},

	title: {
		flex: 1,
		flexDirection: 'row',
		marginLeft: 10,
		fontWeight: 'bold',
	},

	itemText: {
		flex: 1,
		marginLeft: 10,
	},

	modal: {
		flex: 1,
		marginLeft: 30,
		marginRight: 30,
		justifyContent: 'center',
	},

	input: {
        marginBottom: 10,
        paddingHorizontal: 8,
        paddingVertical: 6,
        borderBottomWidth: 1,
        borderBottomColor: '#ddd'
    }
})

